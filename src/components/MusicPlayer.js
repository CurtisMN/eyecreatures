import React, { Fragment } from 'react';

console.log('The embedded players cause console errors. TODO: update the page to use different audio/video players')

const MusicPlayer = (props) => (
  <Fragment>
    <div>
      <iframe
        title="video"
        style={{ height: '450px' }}
        src="https://www.youtube.com/embed/sdTPiTM6AWA"
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
    </div>
    <br />
    <br />
    <iframe
      width="100%"
      height="166"
      scrolling="no"
      frameBorder="no"
      allow="autoplay"
      title="static"
      src={
        'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/512953275&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true'
      }
    />
    <iframe
      width="100%"
      height="166"
      scrolling="no"
      frameBorder="no"
      allow="autoplay"
      title="thecube"
      src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/512953200&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"
    />
    <iframe
      width="100%"
      height="450"
      scrolling="no"
      frameBorder="no"
      allow="autoplay"
      title="playlist"
      src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/616057947&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"
    />
  </Fragment>
);

export default MusicPlayer;
