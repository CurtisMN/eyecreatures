import React, { Fragment } from 'react';
import { Global } from '@emotion/core';

import ArtStyles from '../assets/css/artgallery';

export const ArtGallery = () => (
  <Fragment>
    <Global styles={ArtStyles} />
    <div className="gallery">
      <img src={require('../images/art/1.jpg')} alt="art" />
    </div>
    <div className="gallery">
      <img src={require('../images/art/2.jpg')} alt="art" />
    </div>
    <div className="gallery">
      <img src={require('../images/art/3.jpg')} alt="art" />
    </div>
    <div className="gallery">
      <img src={require('../images/art/4.jpg')} alt="art" />
    </div>
    <div className="gallery">
      <img src={require('../images/art/5.jpg')} alt="art" />
    </div>
    <div className="gallery">
      <img src={require('../images/art/6.jpg')} alt="art" />
    </div>
  </Fragment>
);

export default ArtGallery;
