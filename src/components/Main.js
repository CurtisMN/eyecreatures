import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GigTable from '../components/GigTable';
import ArtGallery from '../components/ArtGallery';
import MusicPlayer from '../components/MusicPlayer';

class Main extends Component {
  render() {
    let close = (
      <div
        className="close"
        onClick={() => {
          this.props.onCloseArticle();
        }}
      />
    );

    return (
      <div
        ref={this.props.setWrapperRef}
        id="main"
        style={this.props.timeout ? { display: 'flex' } : { display: 'none' }}
      >
        <article
          id="intro"
          className={`${this.props.article === 'gigs' ? 'active' : ''} ${
            this.props.articleTimeout ? 'timeout' : ''
          }`}
          style={{ display: 'none' }}
        >
          <GigTable />
          <br />
          <br />
          {close}
        </article>

        <article
          id="work"
          className={`${this.props.article === 'tunes' ? 'active' : ''} ${
            this.props.articleTimeout ? 'timeout' : ''
          }`}
          style={{ display: 'none' }}
        >
          <h2 className="major">Tunes</h2>
          <br />
          <MusicPlayer />
          <br />
          <br />
          <p
            style={{
              color: '#FB667A',
              borderTop: '1px',
              borderTopColor: 'red',
            }}
          >
            WARNING: THESE SONGS MAY CAUSE̋͑͏̲̟̖͍͜͜ ̴ͫ̓̓̅ͩ̿̊̊̿҉̢͚̳̦̫ͅ ̴ͫ̓̓̅ͩ̿̊̊̿҉̢͚̳̦̫ͅ ͙͖̿́ ̛̹͖̉ͬͤͧ̆̍͑ͮ ̷̲̼̜̫̫̲̻̺͂ͫ̈́ͦ̏͐ ̶̡̦͍̯̋ ̦͎ͮͦͨ̆̔Ī̢̺͓̙̪̘̰͕͈̯̽͆͗̋͂ͭͭ͘ ̱͇̬̣̻͉̋ͫ̓͊̏̍ͮ ̡̡̫͒ͮͦ̐ͤ͑̒̀͢I̗̞͈̦͍ͥ̓ͧ͢ͅS̴̼̪͉̎̈̏͐̇ͯͅ IN LOWER LIFE FORMS
          </p>
          {close}
        </article>

        <article
          id="about"
          className={`${this.props.article === 'art' ? 'active' : ''} ${
            this.props.articleTimeout ? 'timeout' : ''
          }`}
          style={{ display: 'none' }}
        >
          <h2 className="major">Art</h2>
          <ArtGallery />
          {close}
        </article>

        <article
          id="contact"
          className={`${this.props.article === 'contact' ? 'active' : ''} ${
            this.props.articleTimeout ? 'timeout' : ''
          }`}
          style={{ display: 'none' }}
        >
          <h2 className="major">Make Contact</h2>
          <form name="contact" method="post" data-netlify="true" data-netlify-honeypot="bot-field">
            <input type="hidden" name="bot-field" />
            <p>
              <label>
                Email: <input type="text" name="email" />
              </label>
            </p>
            <p>
              <label>
                Message: <textarea type='textarea' name="message" />
              </label>
            </p>
            <p>
              <button type="submit" name="button">Send</button>
            </p>
          </form>
          <ul className="icons">
            <li>
              <a
                href="https://open.spotify.com/artist/652jOvfVIuxXvCeuaxbiAD"
                className="icon fa-spotify"
              >
                <span className="label">Spotify</span>
              </a>
            </li>
            <li>
              <a
                href="https://www.facebook.com/Eye-Creatures-125727898114171/"
                className="icon fa-facebook"
              >
                <span className="label">Facebook</span>
              </a>
            </li>
            <li>
              <a href="https://soundcloud.com/eyecreatures" className="icon fa-soundcloud">
                <span className="label">Soundcloud</span>
              </a>
            </li>
            <li>
              <a
                href="https://www.youtube.com/channel/UCqdBNhfSsEQIw4seGM3-zqA"
                className="icon fa-youtube"
              >
                <span className="label">Youtube</span>
              </a>
            </li>
          </ul>
          {close}
        </article>
      </div>
    );
  }
}

Main.propTypes = {
  route: PropTypes.object,
  article: PropTypes.string,
  articleTimeout: PropTypes.bool,
  onCloseArticle: PropTypes.func,
  timeout: PropTypes.bool,
  setWrapperRef: PropTypes.func.isRequired,
};

export default Main;
