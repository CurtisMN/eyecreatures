import React, { Fragment } from 'react';
import { Global } from '@emotion/core';

import ArtStyles from '../assets/css/gigtable';

const GigTable = () => (
  <Fragment>
    <Global styles={ArtStyles} />
    <h1>
      <span className="blue">Gigs</span>
    </h1>

    <table className="container">
      <thead>
        <tr>
          <th>
            <h1>Date</h1>
          </th>
          <th>
            <h1>Location</h1>
          </th>
          <th>
            <h1>Venue</h1>
          </th>
          <th>
            <h1>Pricing</h1>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>March 9, 9pm-12am</td>
          <td>Joplin, MO</td>
          <td>The AuxPort</td>
          <td>Free</td>
        </tr>
      </tbody>
    </table>
  </Fragment>
);

export default GigTable;
