import { css } from '@emotion/core';

export default css`
  gallery:hover {
    border: 1px solid #777;
  }

  img {
    height: 100%;
    width: 100%;
    object-fit: contain;
  }

  iframe {
    height: 100%;
    width: 100%;
    object-fit: contain;
  }

  desc {
    padding: 15px;
    text-align: center;
  }
`;
